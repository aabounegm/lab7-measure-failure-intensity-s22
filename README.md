# Lab8 - measure failure intensity

## Introduction

Ok, during the last lab we were doing fuzz testing, this is a good tool to understand which inputs may break your app. But how to understand if you need to quality maitenance, and call an ambulance to save your product. There is the only way to understand it, you need to measure failure intensity of your product, and guess what we are going to do today. ***Let's roll!***

## Failure intensity

Well, this is a time when the name speaks for itself. Failure intensity is an amount of failures on your service during the unit of time(or code). Measuring it will help you to understand if something wrong goes with your service, to check it you may count number of 503/404 and other responses, compared to your normal 200s. We will talk about load testing later, but for now lets keep it simple.

## Lab

Ok, let's crush all the stuff out of it:
1. Create your fork of the `
Lab7 - Failure intensity
` repo and clone it. [***Here***](https://gitlab.com/sqr-inno/lab7-measure-failure-intensity-s22)
2. For todays lab you will need Apache Benchmark, on linux you can install it using shell command:
```sh
sudo apt install apache2-utils
```
3. And for testing we need some app, just use link from previous lab.
4. To test it you should run command like this one:
```sh
ab -n 20000 -c 100 -m "GET" _url_
```
This means that we are going to send 20000 requests, with 100 of them sending concurrently to the server. We have few lines like `Failed requests` and `Non-2xx responses`, which are exactly failures we're searching here. To measure the exact metric you should use just this formule, where MTTF is time between failures:
`FI = 1 / MTTF`.

## Homework

As a homework you will need to test your service(GetSpec request of InnoDrive from previous labs) and provide the results of your test as a screenshot when failure intensity is equal to the `0.2+-5%`. + provide the calculations for the failure intensity

## Solution

Command ran:
```sh
ab -n 20000 -c 400 -m "GET" "https://script.google.com/macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=a.abounegm@innopolis.university"
```

### Results

```math
MTTF = \frac{test\_time}{failure\_count} = \frac{11,250 ms}{1859 + 141} = 5.625 ms
```

```math
FI = \frac{1}{MTTF} = \frac{failure\_count}{test\_time} = \frac{1}{5.625 ms} = 0.1778 \approx 0.2
```

### Screenshot

![report](./report.png)

### Full raw output

```
This is ApacheBench, Version 2.3 <$Revision: 1843412 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking script.google.com (be patient)
Completed 200 requests
Completed 400 requests
Completed 600 requests
Completed 800 requests
Completed 1000 requests
Completed 1200 requests
Completed 1400 requests
Completed 1600 requests
Completed 1800 requests
Completed 2000 requests
Finished 2000 requests


Server Software:        GSE
Server Hostname:        script.google.com
Server Port:            443
SSL/TLS Protocol:       TLSv1.2,ECDHE-ECDSA-CHACHA20-POLY1305,256,256
Server Temp Key:        X25519 253 bits
TLS Server Name:        script.google.com

Document Path:          /macros/s/AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w/exec?service=getSpec&email=a.abounegm@innopolis.university
Document Length:        621 bytes

Concurrency Level:      400
Time taken for tests:   11.250 seconds
Complete requests:      2000
Failed requests:        1859
   (Connect: 0, Receive: 0, Length: 1859, Exceptions: 0)
Non-2xx responses:      141
Total transferred:      2547298 bytes
HTML transferred:       1394171 bytes
Requests per second:    177.78 [#/sec] (mean)
Time per request:       2250.015 [ms] (mean)
Time per request:       5.625 [ms] (mean, across all concurrent requests)
Transfer rate:          221.12 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:      106  351 176.9    313    1492
Processing:   303  560 432.1    477    7747
Waiting:      301  548 431.3    457    7746
Total:        449  911 470.3    846    8949

Percentage of the requests served within a certain time (ms)
  50%    846
  66%    943
  75%   1000
  80%   1055
  90%   1289
  95%   1454
  98%   1674
  99%   1793
 100%   8949 (longest request)
```
